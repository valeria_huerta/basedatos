/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usr
 */
public class BaseDatos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BaseDatos bd = new BaseDatos();
        bd.conectar();
        
    }
    
    public void conectar() {
        //Definimos el conector a la base de datos
        String driver = "com.mysql.jdbc.Driver";
        //El nombre de la base de datos
        String database = "sakila";
        //El Usuario
        String user = "root";
        //La Contraseña
        String password = "nada";
        //El puerto de conexion
        String port = "3306";
        //El servidor
        String server = "localhost";
        //La url de conexión
        //jdbc:mysql://localhost:3306/sakila
        String url = "jdbc:mysql://" + server + ":" + port
                + "/" + database;
        
        Connection con = null;
        
        try{
            Class.forName(driver);
            con = (Connection) DriverManager.getConnection(url, user, password);//conectarnos
        } catch (ClassNotFoundException ex) {
            System.out.println("No se encontró el conector");
            Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        } catch(SQLException ex){
            System.out.println("No se puede conectar a la base de datos");
        }finally{ //se ejecuta indep si se eligio o no
            //Cerramos la conexion a la base de datos
            if(con != null){
                try{
                    con.close();
                }catch(SQLException ex){
                    Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
